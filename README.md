# fmtstr
**fmtstr** is a very **simple** and **lightweight** string formatting tool. Its
desired use is there, where GNU Autosprintf would be inappropriate (too large,
too complicated, too requiring, and so on). The simple usage can be expressed
as following:

	#include <cmath>
	#include <fmtstr.h>
	#include <iostream>
	
	using namespace std;
	
	int
	main (int argc, char **argv)
	{
	  cout << fmtstr ("%0; and %1; - important constants", {M_PI, M_E}) << endl;
	}

As you can see, the only rule found is that sequences of form `%NUM;`. This
indicates an index to later initializer list. No bound checking is currently
done; but this will be added sometime.

# Contribution guide
If you want to contribute, **you are welcome**! I accept help of any kind, but,
to simplify my task, I ask you, to send a pull request with following contents:

	Merging <USER>/<branch> into KubaKaszycki/<branch>

	Visible changes:
	 - now, it no longer looks dumb
	 - GUI has been packed
	
	New features:
	 - processing XML files as YAML files
	 - reformatting PNG files to use the coding style
	
	Removed features:
	 - integration with KDE (because it uses unformatted PNG files)
	 - processing XML files natively (they are now mapped to YAML)
	
	Broken features:
	 - compilation with libyaml (done some macro mash, which completely broke it)

There are some very important things about that:

 1. Try to make the "new features" list the longest possible, and "removed
 features" and "broken features" short.
 2. Justify every feature removal
 3. Prove that breaking existing features was mandatory.
 4. If your patch removes nearly all functionality just to define one more
 macro, it will be rejected

#define __INSIDE_FMTSTR
#include <fmtstr.h>

#include <initializer_list>
#include <vector>

namespace __fmtstr
{
  std::string
  fmtstr (const std::string& fmt, std::initializer_list<__tostringable> args)
    throw (std::invalid_argument)
  {
    std::string outbuf;
    std::string tempbuf;
    std::vector<__tostringable> argv(args.begin (), args.end ());

    for (auto itr = fmt.cbegin (); itr != fmt.cend (); itr++)
      {
        switch (*itr)
          {
          case '%':
            if (itr == fmt.cend ())
              {
                outbuf += '%';
                goto mainloop_after;
              }
            itr++;
            if (*itr == '%')
              {
                outbuf += '%';
                continue;
              }
            else if (isdigit (*itr))
              {
                // Read whole number into the array
                tempbuf += ((char) *itr);
                while (true)
                  {
                    if (itr == fmt.cend ())
                      {
                        outbuf += tempbuf;
                        goto mainloop_after;
                      }
                    itr++;
                    if (*itr == ';')
                      break;
                    if (!isdigit (*itr))
                      {
                        outbuf += tempbuf;
                        goto mainloop_after;
                      }
                    tempbuf += ((char) *itr);
                  }
                unsigned int index = std::atoi (tempbuf.c_str ());
                tempbuf.clear ();
                outbuf += argv[index].__value;
              }
            break;
          default:
            outbuf += ((char) *itr);
          }
      }
mainloop_after: 
    return outbuf;
  }
}

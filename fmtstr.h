// This file is -*- C++ -*-
// vim: ft=cpp

#ifndef FMTSTR_H
#define FMTSTR_H

#include <stdexcept>
#include <string>

namespace __fmtstr
{
  class __tostringable
  {
  public:
    // TODO: Add more constructors
    inline
    __tostringable (void) noexcept
    {
      __value = "(null)";
    }

    inline
    __tostringable (const __tostringable& other) noexcept
    {
      __value = other.__value;
    }

#define DIRECT_ASSIGN_CONSTRUCTOR(x) \
    inline \
    __tostringable (x obj) noexcept \
    { \
      __value = obj; \
    }

    DIRECT_ASSIGN_CONSTRUCTOR (const std::string&);
    DIRECT_ASSIGN_CONSTRUCTOR (std::string&&);
    DIRECT_ASSIGN_CONSTRUCTOR (const char *);
    DIRECT_ASSIGN_CONSTRUCTOR (char *);

#undef DIRECT_ASSIGN_CONSTRUCTOR

#define TOSTRING_CONSTRUCTOR(x) \
    inline \
    __tostringable (x obj) noexcept \
    { \
      __value = std::to_string (obj); \
    }

    TOSTRING_CONSTRUCTOR (bool);
    TOSTRING_CONSTRUCTOR (float);
    TOSTRING_CONSTRUCTOR (double);
    TOSTRING_CONSTRUCTOR (long double);

#define INTTYPE_CONSTRUCTOR(t) \
    TOSTRING_CONSTRUCTOR (unsigned t); \
    TOSTRING_CONSTRUCTOR (signed t);

    INTTYPE_CONSTRUCTOR (char);
    INTTYPE_CONSTRUCTOR (short int);
    INTTYPE_CONSTRUCTOR (int);
    INTTYPE_CONSTRUCTOR (long int);
    INTTYPE_CONSTRUCTOR (long long int);

#undef INTTYPE_CONSTRUCTOR
#undef TOSTRING_CONSTRUCTOR

    inline virtual
    ~__tostringable (void) noexcept
    {
    }

    std::string __value;
  };

  std::string fmtstr (const std::string&,
                      std::initializer_list<__tostringable>) throw
    (std::invalid_argument);
}

using __fmtstr::fmtstr;

#endif

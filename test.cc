#include <fmtstr.h>

#include <cmath>
#include <iostream>

using namespace std;

int
main (void)
{
  cout << fmtstr ("E equals %1;, Pi equals %0;. 7 is %2;%% of 10.", { M_PI, M_E, 70 }) << endl;
  return 0;
}
